
import fs from 'fs';
import HTMLParser from 'node-html-parser';
import path from 'path';
import csso from 'csso';

// npm --tpl-inline-styles run build
let inlineStyles = process.env.npm_config_tpl_inline_styles ? process.env.npm_config_tpl_inline_styles : false;
let styleCache = new Map();



export function inlineStylesToTemplate(name, content) {

    let themeName = name.match('sk-(.*)') ? name.match('sk-(.*)')[1] : name.match('gridy-grid-(.*)')[1];
    let dom = HTMLParser.parse(content);
    let links = dom.querySelectorAll('link[rel=stylesheet]');
    for (let link of links) {
        let href = link.getAttribute('href');
        let tokens = href.split('/');
        let fileName = tokens[tokens.length - 1];
        let filePath;
        if (typeof inlineStyles === 'string' && inlineStyles !== 'true') {
            let stylePaths = inlineStyles.split(',');
            for (let stylePath of stylePaths) {
                let checkFilePath = path.join(stylePath, fileName);
                if (fs.existsSync(checkFilePath)) {
                    filePath = checkFilePath;
                }
            }
        } else {
            filePath = path.join("node_modules", `sk-theme-${themeName}`, fileName);
        }
        let minifiedCss = '';
        if (! styleCache.get(filePath)) {
            if (fs.existsSync(filePath)) {
                let styleContent = fs.readFileSync(filePath, 'utf8');
                let ast = csso.syntax.parse(styleContent);
                let compressedAst = csso.syntax.compress(ast, {comments: false}).ast;
                minifiedCss = csso.syntax.generate(compressedAst);
                styleCache.set(filePath, minifiedCss);
            }
        } else {
            minifiedCss = styleCache.get(filePath);
        }
        if (minifiedCss !== '') {
            link.insertAdjacentHTML('afterend', `<style data-file-name="${fileName}">${minifiedCss}</style>`);
            link.remove();
        } else {
            console.warn(`Style file ${filePath} not found`);
        }
    }
    return dom.outerHTML;
}

export function bundleTemplates(srcConfPath = "templates.json", name = 'default', bundleType = 'native') {

    let templateConfig = JSON.parse(fs.readFileSync(srcConfPath, 'utf8'));
    console.log(`Building templates bundle for ${name} theme`);
    console.log('inlineStyles: ', inlineStyles);
    let bundleName = bundleType === 'native' ? `${name}-tpls.html` : `${name}-tpls-${bundleType}.html`;
    let templates = Object.keys(templateConfig);
    let writeStream = fs.createWriteStream('dist/' + bundleName);
    for (let templateId of templates) {
        let tplType = bundleType;
        let templateCfg = templateConfig[templateId];
        let content, templatePath;
        if (typeof templateCfg === 'string') {
            templatePath = templateCfg;
        } else {
            templatePath = templateCfg.tplPath;
            if (templateCfg.tplType) {
                tplType = templateCfg.tplType;
            }
        }
        content = fs.readFileSync(templatePath, 'utf8');
        if (inlineStyles) {
            content = inlineStylesToTemplate(name, content);
        }
        if (tplType === 'hg') {
            writeStream.write(`
<script type="text/x-handlebars-template" id="${templateId}">
${content}
</script>`
            );
        } else {
            writeStream.write(`
<template id="${templateId}">
${content}
</template>`
            );
        }
    }
    writeStream.end();
}

