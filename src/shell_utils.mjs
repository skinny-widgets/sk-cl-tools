
import childProcess from "child_process";

/**
 * @param {string} command A shell command to execute
 * @return {Promise<string>} A promise that resolve to the output of the shell command, or an error
 * @example const output = await execute("ls -alh");
 */
export function execute(command) {
  /**
   * @param {Function} resolve A function that resolves the promise
   * @param {Function} reject A function that fails the promise
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
   */
  return new Promise(function(resolve, reject) {
    /**
     * @param {Error} error An error triggered during the execution of the childProcess.exec command
     * @param {string|Buffer} standardOutput The result of the shell command execution
     * @param {string|Buffer} standardError The error resulting of the shell command execution
     * @see https://nodejs.org/api/child_process.html#child_process_child_process_exec_command_options_callback
     */
    childProcess.exec(command, function(error, standardOutput, standardError) {
      if (error) {
        reject();

        return;
      }

      if (standardError) {
        reject(standardError);

        return;
      }

      resolve(standardOutput);
    });
  });
}


export function replaceVars(target, map) {
    for (let key of Object.keys(map)) {
        target = target.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
    }
    return target;
}

export function format(str) {
    let values = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];
    if (values.length === 1 && typeof values[0] === 'object') {
        return replaceVars(str, values[0]);
    } else {
        return str.replace(/{(\d+)}/g, function (match, number) {
            return typeof values[number] != 'undefined'
                ? values[number]
                : match
                ;
        });
    }
}


