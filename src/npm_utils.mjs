import fs from 'fs';

import * as sh from "./shell_utils.mjs";

import { format } from "./shell_utils.mjs";

import path from "path";
import http from "http";
import os from "os";

import archiver from 'archiver';
import unzipper from 'unzipper';

export async function doForRepos(srcConfPath = "repos.json", includeMask, excludeMask, cmd, ifExists, runProps, source) {
    let repoConfig = JSON.parse(fs.readFileSync(srcConfPath, 'utf8'));
    let packageNames = Object.keys(repoConfig);
    if (! source) {
        source = "../";
    }
    //await sh.execute(`cd ../`);
    
    let includeRegExp = includeMask ? new RegExp(includeMask) : null;
    let excludeRegExp = excludeMask ? new RegExp(excludeMask) : null;
    let results = []
    let listDir = fs.readdirSync(source);
    for (let packageName of packageNames) {
        if ((includeRegExp && packageName.match(includeRegExp)) || includeRegExp === null) {
            if (excludeRegExp && packageName.match(excludeRegExp)) {
                continue;
            }
            let cond = ifExists ? fs.existsSync(`${source}/${packageName}`) : (! fs.existsSync(`${source}/${packageName}`));
            if (ifExists === null) {
                cond = true;
            }
            if (cond) {
                let url = repoConfig[packageName];
                console.log(`trying with ${packageName}, url: ${url}`);
                let initData = { url: url, packageName: packageName };
                try {
                    if (typeof cmd === "function") {
                        let result  = await cmd.call(initData);
                        console.log('result', `${result}`);
                        results.push(result);
                    } else if (cmd === 'pwd') {
                        let result = path.resolve(`${source}/${packageName}`);
                        if (fs.existsSync(result)) {
                            console.log('result', `${result}`);
                            results.push(result);
                        } else {
                            for (let file of listDir) { // find similar, e.g. with version
                                if (file.match(packageName)) {
                                    result = path.resolve(`${source}/${file}`);
                                    console.log('result', `${result}`);
                                    results.push(result);
                                }
                            }
                        }
                    } else {
                        let newProps = {};
                        if (! runProps) {
                            runProps = {};
                        }
                        for (let runProp of Object.keys(runProps)) {
                            if (typeof runProps[runProp] === 'function') {
                                newProps[runProp] = runProps[runProp].call(initData);
                            } else {
                                newProps[runProp] = runProps[runProp]
                            }
                        }
                        let ctx = Object.assign(initData, newProps);
                        let fmtedCmd = format(cmd, ctx);
                        console.log(`executing ${fmtedCmd}`);
                        const result = await sh.execute(fmtedCmd);
                        results.push(result);
                        console.log(`done with ${packageName}`);
                    }
                    
                } catch (e) {
                    console.log(`error with ${packageName}`, e);
                    console.error(e);
                }
            } else {
                console.log(`${packageName} at ${source} workcopy does not met condition, skiping`);
            }
        }
    }
    return Promise.resolve(results);
}

export async function localInstallReposTo(srcConfPath = "repos.json", includeMask, excludeMask, target, source) {
    console.info(`install local repos to ${target}:`);
    if (! source) {
        source = "../";
    }
    /*    let paths = await doForRepos(srcConfPath, includeMask, excludeMask,
			"cd " + source + " && cd {{ packageName }} && pwd | tr -d '\n'", true, {}, source);*/
    let paths = await doForRepos(srcConfPath, includeMask, excludeMask,
        "pwd", true, {}, source);
    console.debug('paths', paths);
    //:TODO if tgz of any version is matched it's installed from tgz, else if dir then dir is installed
    // :NOTE where to take version for tgz ? from list dir with mask ?
    let packagesInstallCmd = paths.map((path) => `npm i file://${path}`).join(' && ');
    try {
        const result = await sh.execute(`cd ${target} && ${packagesInstallCmd}`);
        console.log(`done with installing ${result}`);
    } catch (e) {
        console.error(`error with executing: cd ${target} && ${packagesInstallCmd}`, e);
    }
}

export async function localInstallReposFromTgz(srcConfPath = "repos.json", includeMask, excludeMask, target, source) {
    console.info(`install local repos to ${target}:`);
    if (! source) {
        source = "../";
    }
    let paths = [];
    if (source.endsWith('.zip')) {
        let tmpTarget = fs.mkdtempSync(path.join(os.tmpdir(), "sk-cl-tools"));
        if (! tmpTarget.endsWith('/')) {
            tmpTarget += '/';
        }
        await unzipDirectory(source, tmpTarget);
        paths = await doForRepos(srcConfPath, includeMask, excludeMask,
            "pwd", null, {}, tmpTarget);
        
        let packagesInstallCmd = paths.map((path) => `npm i file://${path}`).join(' ; ');
        console.debug('cmd', packagesInstallCmd);
        try {
            const result = await sh.execute(`cd ${target} && ${packagesInstallCmd}`);
            console.log(`done with installing ${result}`);
        } catch (e) {
            console.error(`error with executing: cd ${target} && ${packagesInstallCmd}`, e);
        } finally {
            fs.rmdirSync(tmpTarget, { recursive: true });
        }

    } else {
        /*    let paths = await doForRepos(srcConfPath, includeMask, excludeMask,
				"cd " + source + " && cd {{ packageName }} && pwd | tr -d '\n'", true, {}, source);*/
        // :TODO get versions from package.json
        paths = await doForRepos(srcConfPath, includeMask, excludeMask,
            "npm i " + source + "/{{ packageName }}.tgz", false, {}, source);
        console.debug('paths', paths);
    }


}

export async function downloadUrl(url, target) {
    return new Promise((resolve, reject) => {
        let parts = url.split("/");
        let fileName = parts[parts.length - 1];
        if (! fs.existsSync(path.resolve(`${target}${fileName}`))) {
            console.log(`Download ${url} to ${target}${fileName}`);
            const file = fs.createWriteStream(`${target}${fileName}`);
            try {
                http.get(url, function (response) {
                    response.pipe(file);
                    resolve(file);
                }).on('error', (e) => {
                    console.error(`url download error for url: ${url}: ${e.message}`);
                    reject(e);
                });
            } catch (e) {
                reject(e);
            }
        } else {
            console.log(`File ${target}${fileName} exists, skipping`);
            resolve();
        }
    });

}

export async function zipDirectory(sourceDir, outPath) {
    const archive = archiver('zip', { zlib: { level: 1 }});
    const stream = fs.createWriteStream(outPath);
    
    return new Promise((resolve, reject) => {
        archive
            .directory(sourceDir, false)
            .on('error', err => reject(err))
            .pipe(stream)
        ;
        
        stream.on('close', () => resolve());
        archive.finalize();
    });
}

export async function unzipDirectory(source, target) {
    return new Promise((resolve, reject) => {
        fs.createReadStream(source)
            .pipe(unzipper.Extract({ path: target }))
            .on('close', () => resolve())
            .on('error', e => reject(e));
    });
}

export async function downloadDepPackagesTo(srcConfPath = "repos.json", includeMask, excludeMask, target, source) {
    console.info(`install local repos to ${target}:`);
    if (! source) {
        source = "../";
    }
    /*    let paths = await doForRepos(srcConfPath, includeMask, excludeMask,
			"cd " + source + " && cd {{ packageName }} && pwd | tr -d '\n'", true, {}, source);*/
    let paths = await doForRepos(srcConfPath, includeMask, excludeMask, "pwd", true, {}, source);
    let curPath = path.resolve(`.`);
    if (fs.existsSync(`${curPath}/package-lock.json`)) {
        paths.push(curPath);
    }
    console.debug('paths', paths);
    let targetFilePath = null;
    if (target.match('.zip')) {
        targetFilePath = target;
        target = fs.mkdtempSync(path.join(os.tmpdir(), "sk-cl-tools"));
        if (! target.endsWith('/')) {
            target += '/';
        }
    }
    for (let path of paths) {
        let lockExists = fs.existsSync(`${path}/package-lock.json`);
        if (lockExists) {
            console.log(`${path}/package-lock.json exists`)
            let lockJson = JSON.parse(fs.readFileSync(`${path}/package-lock.json`, 'utf8'));
            for (let depName of Object.keys(lockJson.packages)) {
                let dep = lockJson.packages[depName];
                if (dep.resolved) {
                    try {
                        await downloadUrl(dep.resolved, target);
                    } catch (e) {
                        console.log(e);
                    }
                }
                if (dep.dependencies) {
                    for (let subdepName of Object.keys(dep.dependencies)) {
                        let subdep = dep.dependencies[subdepName];
                        if (subdep.resolved) {
                            try {
                                await downloadUrl(subdep.resolved, target);
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    }
                }
            }
        }
    }
    if (targetFilePath) {
        await zipDirectory(target, targetFilePath);
        fs.rmdirSync(target, { recursive: true });
    }
}

export async function publishReposTo(srcConfPath = "repos.json", includeMask, excludeMask, registry) {
    console.info(`publish repos to ${registry}:`);
    if (! registry) {
        registry = 'https://registry.npmjs.org';
    }
    await doForRepos(srcConfPath, includeMask, excludeMask,
        "cd ../ && cd {{ packageName }} && npm publish --registry={{ registry }}", true, { registry: registry });
    
}

export async function buildReposWith(srcConfPath = "repos.json", includeMask, excludeMask, registry) {
    console.info(`build repos with registry: ${registry}`);
    if (! registry) {
        registry = 'https://registry.npmjs.org';
    }
    await doForRepos(srcConfPath, includeMask, excludeMask,
        "cd ../ && cd {{ packageName }} && npm run build --registry={{ registry }}", true, { registry: registry });
    
}

export async function reinstallReposWith(srcConfPath = "repos.json", includeMask, excludeMask, registry) {
    console.info(`reinstall repos with registry: ${registry}`);
    if (! registry) {
        registry = 'https://registry.npmjs.org';
    }
    await doForRepos(srcConfPath, includeMask, excludeMask,
        "cd ../ && cd {{ packageName }} && rm ./node_modules/ -rf && rm -f package-lock.json && npm i --registry={{ registry }}", true, { registry: registry });
    
}

