
import fs from 'fs';
import ver from 'semver';

const getDirectories = source =>
    fs.readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory() && ! dirent.name.startsWith('.'))
        .map(dirent => dirent.name);


export function updateVersions(startPath = "../", newVersion, readonly = false, nameMatch, scanMatch) {
    let dirs = getDirectories(startPath);
    for (let dir of dirs) {
        if (nameMatch && ! dir.match(scanMatch)) { // directory name doesn't match provided pattern, skip it
            continue;
        }
        let writeNeeded = false;
        let packageConfig = JSON.parse(fs.readFileSync(`${startPath}${dir}/package.json` , 'utf8'));
        let packageVersion = packageConfig.version;
        let bestMatch = false;
        if (packageVersion.match(/\^/)) {
            bestMatch = true;
            packageVersion = packageVersion.replace('^', '');
        } else {
            bestMatch = false;
        }
        if (packageVersion !== newVersion && packageConfig.name.match(nameMatch)) {
            console.log(`package ${packageConfig.name} that match name has version mismatch: ${packageVersion}`);
            packageConfig.version = bestMatch ? `^${newVersion}` : newVersion;
            writeNeeded = true;
        }
        if (packageConfig.dependencies) {
            for (let dep of Object.keys(packageConfig.dependencies)) {
                if (dep.match(nameMatch)) {
                    let depVersion = packageConfig.dependencies[dep];
                    let depBestMatch = false;
                    if (depVersion.match(/\^/)) {
                        depBestMatch = true;
                        depVersion = depVersion.replace('^', '');
                    }
                    
                    try {
                        if (!ver.eq(depVersion, newVersion)) {
                            console.log(`dependency ${dep} of package ${packageConfig.name} has version mismatch ${dep}: ${packageConfig.dependencies[dep]} -> ${depBestMatch ? '^'+ newVersion : newVersion}`);
                            packageConfig.dependencies[dep] = depBestMatch ? `^${newVersion}` : newVersion;
                            writeNeeded = true;
                        }
                    } catch (e) {
                        console.log(`something wrong with versions of ${dep}, old: ${depVersion}, new: ${newVersion}, skipping`)
                    }
                }
            }
        }
        if (writeNeeded) {
            if (readonly && readonly !== "false") {
                console.log(`not writing package.json for ${dir} as of read-only mode`);
            } else {
                console.log(`writing package.json for ${dir}`);
                fs.writeFileSync(`${startPath}${dir}/package.json`, JSON.stringify(packageConfig, null, 2));
            }
        }
    }
}
