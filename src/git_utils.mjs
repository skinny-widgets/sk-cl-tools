


import * as nu from "./npm_utils.mjs";

const doForRepos = nu.doForRepos;


export async function checkoutRepos(srcConfPath = "repos.json", includeMask, excludeMask) {

    console.log(`Checkout repos:`);    
    doForRepos(srcConfPath, includeMask, excludeMask, 'cd ../ && git clone {{ url }}', false);

}

export async function commitRepos(srcConfPath = "repos.json", includeMask, excludeMask, message) {
    if (! message) {
        message = "changes sync";
    }
    console.log(`Commit all in repos:`);
    doForRepos(srcConfPath, includeMask, excludeMask, 'cd ../ && cd {{ packageName }} && git add . && git commit -m "' + message + '"', true);
    
}

export async function pushRepos(srcConfPath = "repos.json", includeMask, excludeMask) {
    
    console.log(`Push all in repos:`);
    doForRepos(srcConfPath, includeMask, excludeMask, 'cd ../ && cd {{ packageName }} git pull && git push', true);

}

export async function pullRepos(srcConfPath = "repos.json", includeMask, excludeMask) {
    
    console.log(`Push all in repos:`);
    doForRepos(srcConfPath, includeMask, excludeMask, 'cd ../ && cd {{ packageName }} && git reset --hard HEAD && git pull', true);

}
